using System;
using Xunit;

namespace Struct_Task.Tests
{
    public class UnitTest1
    {
        [Theory]
        [InlineData(1, 2, 6)]
        [InlineData(10, 2, 24)]
        [InlineData(5, 4, 18)]
        public void RectanglePerimeterReturnsCorrectValue(int x, int y, int expected)
        {
            Rectangle r = new Rectangle(x, y);
            int perimeter = 2 * (r.Width + r.Height);

            Assert.Equal(expected, perimeter);
        }
        [Theory]
        [InlineData(-2, 10)]
        [InlineData(12, -7)]
        public void RectangleCreationThrowsException(int x, int y)
        {
            Rectangle r;
            Assert.Throws<ArgumentOutOfRangeException>(() => r = new Rectangle(x, y));
        }
        [Theory]
        [InlineData(-2)]
        [InlineData(0)]
        public void PersonAgeDetectionThrowsException(int x)
        {
            Person p = default;
            Assert.Throws<ArgumentOutOfRangeException>(() => p.AgeDetection(x));
        }
    }
}
