﻿using System;

namespace Struct_Task
{
    public static class Program
    {
        /// <summary>
        /// Start piont of the program
        /// </summary>
        static void Main()
        {
            bool temp = true;
            while(temp)
            {
                Console.WriteLine("To interract with Person struct enter - 1\n" +
                    "To interract with Rectangle struct enter - 2\n" +
                    "To exit press - 3\n");
                int.TryParse(Console.ReadLine(), out int num);
                switch(num)
                {
                    case 1:
                        Console.WriteLine("Enter person name:");
                        string name = Console.ReadLine();
                        Console.WriteLine("Enter person surname:");
                        string surname = Console.ReadLine();
                        Console.WriteLine("Enter person age:");
                        int.TryParse(Console.ReadLine(), out int age);

                        Person person1 = PersonCreation(name, surname, age);

                        Console.WriteLine("Enter age to compare:");
                        int.TryParse(Console.ReadLine(), out int compareAge);
                        person1.AgeDetection(compareAge);

                        break;

                    case 2:
                        Console.WriteLine("Enter x:");
                        int.TryParse(Console.ReadLine(), out int x);
                        Console.WriteLine("Enter y:");
                        int.TryParse(Console.ReadLine(), out int y);

                        Rectangle r = RectangleCreation(x, y);
                        Console.WriteLine($"Perimeter of rectange: {r.Perimeter()} points");

                        break;

                    case 3:
                        temp = false;
                        break;

                    default:
                        Console.WriteLine("You have entered wrong number try again");
                        break;
                }
            }
        }

        /// <summary>
        /// Method to create person
        /// </summary>
        /// <param name="name">Name of the person</param>
        /// <param name="surname">Surname of the person</param>
        /// <param name="age">Age of the person</param>
        /// <returns></returns>
        public static Person PersonCreation(string name, string surname, int age)
        {
            Person p = default;
            p.Name = name;
            p.Surname = surname;
            if (age > 0)
                p.Age = age;

            return p;
        }

        /// <summary>
        /// Method to create rectangle
        /// </summary>
        /// <param name="x">X coordinate</param>
        /// <param name="y">Y coordinate</param>
        /// <returns></returns>
        public static Rectangle RectangleCreation(int x, int y)
        {
            Rectangle r = new Rectangle(x, y);

            return r;
        }
    }
}
