﻿using System;

namespace Struct_Task
{
    /// <summary>
    /// Rectangle structure
    /// </summary>
    public struct Rectangle : ISize, ICoordinates
    {
        /// <summary>
        /// Right top of the rectangle
        /// </summary>
        public int X { get; set; }
        /// <summary>
        /// Bottom top of the rectangle
        /// </summary>
        public int Y { get; set; }
        /// <summary>
        /// Calculates from 0 to x
        /// </summary>
        public int Width { get => X; set => value = X; }
        /// <summary>
        /// Calculates from 0 to y
        /// </summary>
        public int Height { get => Y; set => value = Y; }
        /// <summary>
        /// Constructor to create rectangle
        /// </summary>
        /// <param name="x">X coordinate</param>
        /// <param name="y">Y coordinate</param>
        public Rectangle(int x, int y)
        {
            if(x>0)
                X = x;
            else
                throw new ArgumentOutOfRangeException($"{nameof(x)} must be bigger then 0");
            if(y>0)
                Y = y;
            else
                throw new ArgumentOutOfRangeException($"{nameof(y)} must be bigger then 0");
        }
        /// <summary>
        /// Method to calculate perimeter of the rectangle
        /// </summary>
        /// <returns></returns>
        public double Perimeter()
        {
            return 2 * (Width + Height);
        }
    }
}
