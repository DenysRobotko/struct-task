﻿namespace Struct_Task
{
    /// <summary>
    /// Interface to calculate width, height and perimeter of rectangle
    /// </summary>
    interface ISize
    {
        /// <summary>
        /// Width of rectangle
        /// </summary>
        int Width { get; set; }
        /// <summary>
        /// Height of rectangle
        /// </summary>
        int Height { get; set; }
        /// <summary>
        /// Perimeter ot rectangle
        /// </summary>
        /// <returns></returns>
        double Perimeter();
    }
}
