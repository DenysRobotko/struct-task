﻿namespace Struct_Task
{
    /// <summary>
    /// Position of the right bottom top of the rectanle
    /// </summary>
    interface ICoordinates
    {
        /// <summary>
        /// Right top of the rectangle
        /// </summary>
        int X { get; set; }
        /// <summary>
        /// Bottom top of the rectangle
        /// </summary>
        int Y { get; set; }
    }
}
