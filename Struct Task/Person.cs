﻿using System;

namespace Struct_Task
{
    /// <summary>
    /// Person structure
    /// </summary>
    public struct Person
    {
        /// <summary>
        /// Person's name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Person's surname
        /// </summary>
        public string Surname { get; set; }
        /// <summary>
        /// Person's age
        /// </summary>
        public int Age { get; set; }

        /// <summary>
        /// Check difference between age and parameter
        /// </summary>
        /// <param name="n">Parameter to compare to age</param>
        public void AgeDetection(int n)
        {
            if (n > 0)
            {
                if (Age > n)
                    Console.WriteLine($"{Name} {Surname} older then {n}");
                else if (Age < n)
                    Console.WriteLine($"{Name} {Surname} younger then {n}");
            }
            else
                throw new ArgumentOutOfRangeException("You have entered wrong number.\n" +
                    $"{nameof(n)} must be bigger then 0!");
        }
    }
}
